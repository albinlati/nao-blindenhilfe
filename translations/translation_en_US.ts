<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior.xar:/LineFollower/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>follow me</source>
            <comment>Text</comment>
            <translation type="obsolete">follow me</translation>
        </message>
        <message>
            <source>Follow me!</source>
            <comment>Text</comment>
            <translation type="obsolete">Follow me!</translation>
        </message>
        <message>
            <location filename="behavior.xar" line="0"/>
            <source>Follow me</source>
            <comment>Text</comment>
            <translation type="unfinished">Follow me</translation>
        </message>
    </context>
    <context>
        <name>behavior.xar:/LineFollower/Say (1)</name>
        <message>
            <source>i hit something</source>
            <comment>Text</comment>
            <translation type="obsolete">i hit something</translation>
        </message>
        <message>
            <source>oops</source>
            <comment>Text</comment>
            <translation type="obsolete">oops</translation>
        </message>
        <message>
            <source>there is a Object in front of me</source>
            <comment>Text</comment>
            <translation type="obsolete">there is a Object in front of me</translation>
        </message>
        <message>
            <source>DANGER! OBJECT!</source>
            <comment>Text</comment>
            <translation type="obsolete">DANGER! OBJECT!</translation>
        </message>
        <message>
            <location filename="behavior.xar" line="0"/>
            <source>Danger object</source>
            <comment>Text</comment>
            <translation type="unfinished">Danger object</translation>
        </message>
    </context>
    <context>
        <name>behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>I dont understand you</source>
            <comment>Text</comment>
            <translation type="obsolete">I dont understand you</translation>
        </message>
    </context>
    <context>
        <name>behavior.xar:/Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>arrived</source>
            <comment>Text</comment>
            <translation type="obsolete">arrived</translation>
        </message>
        <message>
            <location filename="behavior.xar" line="0"/>
            <source>Finish</source>
            <comment>Text</comment>
            <translation type="unfinished">Finish</translation>
        </message>
    </context>
    <context>
        <name>behavior.xar:/Say (2)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>I'm lost</source>
            <comment>Text</comment>
            <translation type="obsolete">I'm lost</translation>
        </message>
    </context>
</TS>
